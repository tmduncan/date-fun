# Date FUN

## How to run tests
```
./gradlew test
```


## Organization
```
.
├── README.md
├── build.gradle
├── gradle
│   └── wrapper
│       ├── gradle-wrapper.jar
│       └── gradle-wrapper.properties
├── gradlew
├── gradlew.bat
├── settings.gradle
└── src
    ├── main
    │   ├── java
    │   │   └── dateFun
    │   │       └── DateFun.java
    │   └── resources
    └── test
        ├── java
        │   └── dateFun
        │       └── DateFunTest.java
        └── resources

```



